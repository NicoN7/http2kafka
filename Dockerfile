FROM python:3.10-slim-buster

RUN apt update && apt install -y build-essential

RUN pip install -U pip
RUN pip install poetry

# Install project-specific apt dependencies

RUN mkdir -p /app/src
WORKDIR /app/src

ADD poetry.lock pyproject.toml /app/src/
RUN poetry config virtualenvs.in-project true
RUN poetry install -vv --no-dev
RUN poetry install -vv
ENV PATH="/app/src/.venv/bin:$PATH"

ADD . /app/src/

