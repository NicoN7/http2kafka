#!/bin/bash

CONF_APP_NAME="app"
BASE_DIR=$(dirname "$BASH_SOURCE")
BASE_DIR=$(dirname "$BASE_DIR")
export PYTHONPATH=$PYTHONPATH:$BASE_DIR
exec /usr/bin/env gunicorn $CONF_APP_NAME.wsgi -b 0.0.0.0:8000 --preload --worker-class gthread --workers 1 --threads 5 --max-requests 10000 --max-requests-jitter 800 --logger-class eventy.integration.gunicorn.Logger

