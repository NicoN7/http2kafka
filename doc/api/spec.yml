openapi: 3.1.0
info:
  title: |
    HTTP2Kafka API specification. 
  description: |
    This API is meant to store data (can be messages, events) received as string into kafka.
    One use case of the HTTP2Kafka service is to receive in input events already avro-serialized by eventy on the wizzilab gateway side, and then stores them into kafka.
  version: "1.0.0"
  contact: 
    name: Nicolas Foin
    email: nicolas@foin-dev-web.fr

paths:
  /api/v1/{topic}:
    post:
      summary: Writes a message into a the specified kafka topic.
      parameters:
        - name: topic
          in: path
          required: true
          schema:
            type: string

        - requestBody:
          required: true
          content:
            text/json:
              schema:
                type: object
                properties:
                  data:
                    description: |
                      If the message represents an event, is should be serialized with Avro.
                      The avro serialization yields a bytes array which must then be encoded into a Base64 string.
                    type: string
                    example: "FgK/AACTnhLkSqUC8pAfWj36uoU0na3V4AN+TRgWH/c5/+l6b28PH/eAqZGIyy6t/YxwTnaC37HHSAVHIhjXKaudlLKBih3bkj1WM33Zs01Yng1gV0uU/fN7NugI3cpIk1xzOuc0icRbqWsVejwY3ZDGvZBEdYzT/9v9kzwjZdTLEwujyoqayxcWaoUamFs7Z8WY3VeROfTHtxWlL90xxtkdKByvpB9IIpl0ytA1U3l1DfzgTr3OvV7H3UbLhZ9i91JtIqFMeRevPjprrCxdYCpe0VLEJAvMbvUIL2IZXs3n910RpEZoEycnU2WrOpxyDgJ8pnu8am/HtaNzdkqOEODbMiqh0BFTE5KPQxakFi4uJTZkN8nigBnNw0gG97P3exDh5FwMG13xlCy6UedQrqknxM7w/4xqRWUucFEclpkmn3coRE2nMoKBinbysCjmaS222iz4fo+oUbdg01sHTrvIgg0/L+6Ni1jpnN+glVpmPOWgwyA4pOvGVkmlvqwtiEi29LeyWf/rTHGaeq/blqyMCLN6n4Qsk2x7pXS7Tg5gwwUBRRpfS6OoVPeHNFPee0TeUBbfOwPeodimATlQp6LCS5g1yMWfNV4bOMIl7zLXMtlqEbjWZBIgu8v7FwzYJSar8zMMUUsxrpez4wOiXelw1i4+Zo4f1vpqnEqnn+1zBDK4QJ8rw4e0RpcXE0/ziYfJecx1RmyxkEQX98Tp8JKFQ/ogVArV4yoc0KxE4A8j6XpW74SVTvryjoEGM+124s1xgqwvZB+Df417QW7crmLaC1H5bQaxgt3jPHnBzQ1KSLV/jtSpCoNrFp4jZLNMbv5pNFBIW3Cb0HKUBxlsEQec5IQPg4ZSd/LQGx0H7GO8zmVPzM4Efqq4oxLWkMrUqGmFkwAE9vursucdlV45hgptgZGn58ZF1bzLhosWU5HUYPPkiBlX6Kmmd0EgXg=="
      responses:
        '200':
          description: OK
        '400':
          description: "Happen if body request is empty, or not in JSON format, or if topic is not specified, or if event data is empty."
        '500':
          description: "The server has encountered a problem and cannot process the event"

  /health:
    get:
      summary: Check if the service is up and running
      description: This endpoint performs a health check to verify the service is operational. Upon successful execution, it returns a response with the status code **200 (OK)** and the message "ok".
      responses:
        '200':
          description: OK
