FROM node:13.12-alpine
RUN npm install redoc-cli -g
ADD /doc/api/spec.yml /tmp/spec.yml
RUN redoc-cli bundle /tmp/spec.yml -o /spec.html

