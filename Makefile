help:
	@echo "make test					- Runs automated test suite"
	@echo "make docker-test				- Runs automated test suite inside an isolated Docker container"
	@echo "make build-docker-server		- Builds the docker image or the whole project to run the service server"
	@echo "make run-docker-server		- Runs the service server inside a Docker container"
	@echo "make typecheck				- Run flake8 error and style checking"
	@echo "make spec					- Run a docker container to generate api specs"
	@echo "make clean           		- Deletes build and test files"


flake8:
	flake8 app

mypy:
	mypy app

typecheck: flake8 mypy

test: typecheck
	TEST=TRUE WRITE_DESTINATION=kafka ./manage.py test

docker-test:
	docker build -t http2kafka_test -f Dockerfile.test .

build-docker-server:
	docker build -t http2kafka -f Dockerfile .

run-docker-server:
	docker run -p 8000:8000 --entrypoint=/bin/bash http2kafka -c /app/src/start.sh

spec:
	$(eval GIT_COMMIT = $(shell git rev-parse HEAD))
	docker build . -f Dockerfile.spec -t $(GIT_COMMIT)
	mkdir -p build
	docker run $(GIT_COMMIT) cat /spec.html > build/api-spec.html

clean:
	rm -fr build dist *.egg-info .mypy_cache
	find . -type d -name '__pycache__' -exec rm -fr {} +

