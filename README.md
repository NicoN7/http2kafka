# Overview of http2kafka service
A simple web server that will convert Http requests into Kafka events. For further information regarding the API specifications, you can take a look at [Gitlab pages](https://qotto.gitlab.io/apps/http2kafka/).

## Contributing
For development and test purposes, the Redis store can be used.

In order to use Redis, you can use the following configuration:
 - **WRITE_DESTINATION**: should be set to `REDIS`.
 - **REDIS_HOST**: Redis server host.
 - **REDIS_PORT**: Redis server port.

## Deployment
The service uses the following environment variables as configuration:

### General configuration
  - TEST: Should be set to `True` if the service is running in test mode. Default is `False`.
  - DEBUG: Should be set to `True` if the service is running in debug mode. Default is `False`.
  - **ALLOWED_HOSTS**: A list of allowed hosts. Default is `*` in test mode.

### Kafka configuration
 - **WRITE_DESTINATION**: Should be set to `KAFKA` in production.
 - **KAFKA_BOOTSTRAP_SERVERS**: A list including the addresses of the Kafka servers.
 - **KAFKA_USERNAME**: The user name required to login to the bootstrap server.
 - **KAFKA_PASSWORD**: The password required for the bootstrap server.
 - **SERVICE_DEFAULT_TOPIC**: The default topic to write that will be used if the topic is not specified in the request.

### Redis configuration
 - REDIS_HOST: Redis server host.
 - REDIS_PORT: Redis server port.

### Logging configuration
 - LOG_LEVEL: The log level of the service. Possible values are `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`. Default is `INFO`.
 - GKE_LOGGING: Should be set to `True` if the service is running on Google Kubernetes Engine. Default is `True`
 - COLORED_LOGGING: Can be set to `True` for local development. Default is `False`.
 - SKIP_HEALTHCHECK_LOGGING: Default is `False`. If set to `True`, the health check logs will be skipped.