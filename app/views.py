# Copyright(c) Qotto, 2024

import json
import logging

from base64 import b64decode

from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseServerError
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET, require_POST

from app.init import event_writer
from app.writer.confluent_kafka import MessagingError

logger = logging.getLogger(__name__)

__all__ = ['health_check', 'write_event']


@csrf_exempt
@require_GET
def health_check(request):
    return HttpResponse('OK')


@csrf_exempt
@require_POST
def write_event(request, topic):
    """
    Writes the provided data to the specified Kafka topic.
    If the topic doesn't exits, it will be created automatically.
    """
    if not request.body:
        logger.error("Received empty body")
        return HttpResponseBadRequest("Request body shall not be empty.", 'text/plain')

    try:
        request_data = json.loads(request.body)
        event_base64_data = request_data.get('data')
        if not event_base64_data:
            logger.error("Event must be specified")
            return HttpResponseBadRequest("Request event shall not be empty.", 'text/plain')
    except json.JSONDecodeError:
        logger.error("Invalid JSON format in request body")
        return HttpResponseBadRequest("Request body should be valid JSON.", 'text/plain')

    logger.info(f"Received a request to write an event to topic {topic}.")
    try:
        event_data = b64decode(event_base64_data.encode('ascii'))
        event_writer.write(topic=topic, data=event_data)
    except (MessagingError, RuntimeError) as e:
        logger.error(f"Unable to write event to topic {topic} due to {e}.")
        return HttpResponseServerError("Event could not be written")

    return HttpResponse(status=200)
