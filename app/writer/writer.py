import logging
from app.writer.confluent_kafka import Cursor, KafkaStore, MessagingError
from app import settings
import redis

logger = logging.getLogger(__name__)

__all__ = ['Writer', 'RedisWriter', 'KafkaWriter']


class Writer:
    """
    Common Writer interface

    This class is a base for RedisWriter and KafkaWriter It should not be instantiated.
    """

    def write(self, topic: str, data: bytes) -> None:
        """
        Writes data (of type bytes) to the storage.

        This method should be implemented by subclasses to define their specific writing behavior.

        Args:
            data (bytes): The data to be written.

        Raises:
            NotImplementedError: This method is intended to be implemented by subclasses.
        """
        raise NotImplementedError("Subclasses must implement write(self, data)")


class RedisWriter(Writer):
    """
    Class to write events into memory. Mainly for tests purpose
    """
    redisWriter: redis.Redis

    def __init__(self):
        self.redisWriter = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT)

        logger.info("Memory writer initialized")
        return

    def write(self, data: bytes, topic: str) -> None:  # type: ignore
        """
        Write data to the specified topic in memory.

        Args:
            topic: The topic to write the data to.
            data: The data to be written (bytes).
        """
        self.redisWriter.rpush(topic, data)

        logger.info(f"Wrote new event into topic {topic}")

    def consumeOne(self, topic: str):
        """
        Consumes and returns the first element from the list in Redis under the provided topic,
        and removes the element from the list.

        Args:
            topic (str): The name of the Redis list to consume from.

        Returns:
            str: The first element from the list, or None if the list is empty.
        """
        element = self.redisWriter.lpop(topic)  # Timeout of 0 for non-blocking

        # Check if an element was retrieved
        if element:
            # Return the first element (decoded from bytes)
            elemStr = element.decode('utf-8')  # type: ignore
            logger.info(f"Popped element: {elemStr}")
            return elemStr
        else:
            # List is empty, return None
            return "Empty topic"


class KafkaWriter(Writer):

    kafka_store: KafkaStore
    """
    Class to write events received into kafka. For production purpose
    """
    def __init__(self):
        """
        Connects to kafka
        """
        logger.info(f"Connecting to kafka server: {settings.KAFKA_BOOTSTRAP_SERVERS}")
        self.kafka_store = KafkaStore(
            group_id='http2kafka',  # in case we want to instantiate a consumer (not in normal use)
            bootstrap_servers=settings.KAFKA_BOOTSTRAP_SERVERS,
            sasl_username=getattr(settings, 'KAFKA_USERNAME'),
            sasl_password=getattr(settings, 'KAFKA_PASSWORD'),
        )

        logger.info("Kafka writer initializing")
        return

    def write(self, topic: str, data: bytes) -> None:  # type: ignore
        """
        Register the topic into kafka, and writes data in this topic
        If the writing fails, the function raises a MessagingError.
        """
        # Register topic
        try:
            self.kafka_store.register_topic(topic, Cursor.ACKNOWLEDGED)
        except MessagingError:
            logger.info(f"Topic {topic} already registered. Ready to be read from")
        self.kafka_store.initialize()

        # non transactional write (immediate)
        try:
            self.kafka_store.write_now(data, topic)  # type: ignore
        except MessagingError:
            logger.info("Error when writing message to kafkaStore with write_now.")
            raise MessagingError("Error. Message not written to kafka")
        return

    def consumeOne(self, topic: str):
        """
        Read a message from the given kafka topic, and remove it from kafaka.

        You might need to wait few seconds before calling this route to start getting messages from a kafka topic just after writing into it.
        """
        # Register topic
        try:
            logger.info(f"Registering topic: {topic}")
            self.kafka_store.register_topic(topic, Cursor.ACKNOWLEDGED)
        except MessagingError:
            logger.info(f"Topic {topic} already registered. Ready to be read from")
        self.kafka_store.initialize()

        # Timeout 1 second if no message to be read
        msg_list = self.kafka_store.read(max_count=1, timeout_ms=1000)

        msg = "No message available"
        for m in msg_list:
            msg = m

        return msg
