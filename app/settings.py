# Copyright (c) Qotto, 2024

from enum import Enum
from pathlib import Path

from environs import Env

env = Env()
env.read_env()

# Tests & Logs
TEST = env.bool('TEST', False)
DEBUG = env.bool('DEBUG', False)

LOG_LEVEL: str = env.enum(
    'LOG_LEVEL',
    type=Enum('LOG_LEVEL', 'DEBUG INFO WARNING ERROR CRITICAL'),
    ignore_case=True,
    default='INFO',
).name

GKE_LOGGING: bool = env.bool('GKE_LOGGING', True)
COLORED_LOGGING: bool = env.bool('COLORED_LOGGING', False)
SKIP_HEALTHCHECK_LOGGING: bool = env.bool('SKIP_HEALTHCHECK_LOGGING', False)

# HTTP2KAFKA SETTINGS
WRITE_DESTINATION = env.enum(
    'WRITE_DESTINATION',
    type=Enum('WRITE_DESTINATION', 'REDIS KAFKA'),
    ignore_case=True,
    default='KAFKA',
).name

# Kafka configuration
if WRITE_DESTINATION == 'KAFKA':
    KAFKA_BOOTSTRAP_SERVERS = [  # list of kafka host separated by « , »
        host.strip() for host in env.str('KAFKA_BOOTSTRAP_SERVERS', '').split(',')
    ]
    KAFKA_USERNAME = env.str('KAFKA_USERNAME', '')
    KAFKA_PASSWORD = env.str('KAFKA_PASSWORD', '')
    SERVICE_DEFAULT_TOPIC = env.str('SERVICE_DEFAULT_TOPIC', '')

# Redis config
if WRITE_DESTINATION == 'REDIS':
    REDIS_HOST = env.str('REDIS_HOST', '')
    REDIS_PORT = env.int('REDIS_PORT', 6379)

# DEFAULT DJANGO PROJECT SETTING
BASE_DIR = Path(__file__).resolve().parent.parent

SECRET_KEY = env.str('SECRET_KEY', 'insecure-key' if TEST else '')
if not SECRET_KEY:
    raise EnvironmentError("SECRET_KEY must be set in the production environment.")

ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', ['*'] if TEST else [])

# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'app'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'eventy.integration.django.django_trace_middleware',
]

ROOT_URLCONF = 'app.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'app.wsgi.application'

# Internationalization
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_TZ = True


# Static files (CSS, JavaScript, Images)
STATIC_URL = 'static/'
