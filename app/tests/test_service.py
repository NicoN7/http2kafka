from unittest.mock import patch
from django.test import SimpleTestCase

from app.writer.confluent_kafka import Cursor
from app.writer.writer import KafkaWriter


class TestTopicWrite_Service(SimpleTestCase):
    """
    Test different settings for write_event api route:
        - Without a topic specified
        - With empty event body
        - with empty event data in request body
        - With body, but without data property in it
        - With correct setting to write
    """

    @patch('app.writer.writer.KafkaStore')
    def test_write_event_ok(self, mock_kafka_store) -> None:
        # Return None to avoid unintended consequences
        mock_kafka_store.return_value.initialize.side_effect = lambda *args, **kwargs: print("Mock KafkaStore has initialized")

        event_writer = KafkaWriter()
        event_writer.write('toptopic', 'eventData'.encode('utf-8'))

        # Check KafkaStore.initialize() is called
        mock_kafka_store.return_value.write_now.assert_called_with('eventData'.encode('utf-8'), 'toptopic')
        # Check KafkaStore.register_topic() is called
        mock_kafka_store.return_value.register_topic.assert_called_with('toptopic', Cursor.ACKNOWLEDGED)
