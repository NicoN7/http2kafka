from django.test import SimpleTestCase

topic = 'h2k_topic_test'
serialized_event_data = "FgK/AACTnhLkSqUC8pAfWj36uoU0na3V4AN+TRgWH/c5/+l6b28PH/eAqZGIyy6t/YxwTnaC37HHSAVHIhjXKaudlLKBih3bkj1WM33Zs01Yng1gV0uU/fN7NugI3cpIk1xzOuc0icRbqWsVejwY3ZDGvZBEdYzT/9v9kzwjZdTLEwujyoqayxcWaoUamFs7Z8WY3VeROfTHtxWlL90xxtkdKByvpB9IIpl0ytA1U3l1DfzgTr3OvV7H3UbLhZ9i91JtIqFMeRevPjprrCxdYCpe0VLEJAvMbvUIL2IZXs3n910RpEZoEycnU2WrOpxyDgJ8pnu8am/HtaNzdkqOEODbMiqh0BFTE5KPQxakFi4uJTZkN8nigBnNw0gG97P3exDh5FwMG13xlCy6UedQrqknxM7w/4xqRWUucFEclpkmn3coRE2nMoKBinbysCjmaS222iz4fo+oUbdg01sHTrvIgg0/L+6Ni1jpnN+glVpmPOWgwyA4pOvGVkmlvqwtiEi29LeyWf/rTHGaeq/blqyMCLN6n4Qsk2x7pXS7Tg5gwwUBRRpfS6OoVPeHNFPee0TeUBbfOwPeodimATlQp6LCS5g1yMWfNV4bOMIl7zLXMtlqEbjWZBIgu8v7FwzYJSar8zMMUUsxrpez4wOiXelw1i4+Zo4f1vpqnEqnn+1zBDK4QJ8rw4e0RpcXE0/ziYfJecx1RmyxkEQX98Tp8JKFQ/ogVArV4yoc0KxE4A8j6XpW74SVTvryjoEGM+124s1xgqwvZB+Df417QW7crmLaC1H5bQaxgt3jPHnBzQ1KSLV/jtSpCoNrFp4jZLNMbv5pNFBIW3Cb0HKUBxlsEQec5IQPg4ZSd/LQGx0H7GO8zmVPzM4Efqq4oxLWkMrUqGmFkwAE9vursucdlV45hgptgZGn58ZF1bzLhosWU5HUYPPkiBlX6Kmmd0EgXg=="


class TestTopicWrite_API(SimpleTestCase):
    """
    Test different settings for write_event api route:
        - Without a topic specified
        - With empty event body
        - with empty event data in request body
        - With body, but without data property in it
        - With correct setting to write
    """
    def test_write_event_no_topic_specified(self) -> None:
        json_data = {'data': serialized_event_data}

        response = self.client.post(
            f'/api/v1',
            data=json_data,
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 404)

    def test_write_event_empty_body(self) -> None:
        response = self.client.post(
            f'/api/v1/{topic}',
            content_type='application/json',
        )

        self.assertEqual(response.status_code, 400)

    def test_write_event_topic_empty_data(self) -> None:
        json_data = {'data': ""}

        response = self.client.post(
            f'/api/v1/{topic}',
            data=json_data,
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 400)

    def test_write_event_topic_no_data(self) -> None:
        json_data = {'wrong_property': "full of data"}

        response = self.client.post(
            f'/api/v1/{topic}',
            data=json_data,
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 400)
