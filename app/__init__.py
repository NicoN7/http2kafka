# coding: utf-8
# Copyright (c) Qotto, 2024

# sets up default settings module
import os ; os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app.settings')  # noqa
from app import settings

import logging
import eventy.config
import eventy.config.django
from eventy.logging import GkeHandler, SimpleHandler

# Setup eventy.logging
eventy.config.SERVICE_NAME = 'http2kafka'
# Django integration
eventy.config.django.DJANGO_ACCESS_HEALTH_ROUTE = '/health'
eventy.config.django.DJANGO_ACCESS_DISABLE_HEALTH_LOGGING = settings.SKIP_HEALTHCHECK_LOGGING

# Eventy logging setup
root_logger = logging.getLogger()
root_logger.setLevel(settings.LOG_LEVEL)
root_handler: logging.Handler

if settings.GKE_LOGGING:
    root_handler = GkeHandler()
else:
    root_handler = SimpleHandler(colored=settings.COLORED_LOGGING)

root_logger.addHandler(root_handler)
