# Copyright(c) Qotto, 2024

from app import settings
from app.writer import Writer, RedisWriter, KafkaWriter

__all__ = ['event_writer']

event_writer: Writer
if settings.WRITE_DESTINATION == 'REDIS':
    event_writer = RedisWriter()
elif settings.WRITE_DESTINATION == 'KAFKA':
    event_writer = KafkaWriter()
else:
    raise ValueError(f"Unknown storage destination for events {settings.WRITE_DESTINATION}.")
