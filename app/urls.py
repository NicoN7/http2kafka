# Copyright(c) Qotto, 2024

from django.urls import path
from app import views

urlpatterns = [
    path('api/v1/<str:topic>', views.write_event, name='write_event'),
    path('health', views.health_check, name='health_check'),
]
